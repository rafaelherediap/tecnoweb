# -*- coding: utf-8 -*-
from flask import Flask, request
from models.product import Product

app = Flask(__name__)


@app.route("/")
def index():
    return "Welcome to tecnoweb!"


@app.route("/tecnoweb/search/<int:product_id>")
def search_product(product_id):
    product_model = Product({})
    product_data = product_model.browse(product_id)
    # return str(product_data[0]) + ' ' + product_data[1] + ' ' + product_data[2]
    return str(product_data)


@app.route("/tecnoweb/list_product")
def list_product():
    product_model = Product({})
    product_data = product_model.list_product()
    # return str(product_data[0]) + ' ' + product_data[1] + ' ' + product_data[2]
    return str(product_data)


@app.route("/tecnoweb/searchbymeasure/<int:measure_id>")
def search_productbymeasure(measure_id):
    product_model = Product({})
    product_data = product_model.browsebymeasure(measure_id)
    return str(product_data)


@app.route("/tecnoweb/create", methods=['POST'])
def create_product():
    product_model = Product(request.json)
    product_model.create_product()

    return str('Se creo de forma exitosa el producto')


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=1363)
